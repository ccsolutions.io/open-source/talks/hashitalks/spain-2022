[//]: # (BEGIN_TF_DOCS)
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5.3 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | 2.5.1 |
| <a name="requirement_ionoscloud"></a> [ionoscloud](#requirement\_ionoscloud) | 6.3.3 |
| <a name="requirement_kubectl"></a> [kubectl](#requirement\_kubectl) | 1.14.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.11.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | 2.2.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | 2.5.1 |
| <a name="provider_ionoscloud"></a> [ionoscloud](#provider\_ionoscloud) | 6.3.3 |
| <a name="provider_kubectl"></a> [kubectl](#provider\_kubectl) | 1.14.0 |
| <a name="provider_local"></a> [local](#provider\_local) | 2.2.2 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_datacenter"></a> [datacenter](#module\_datacenter) | git::https://gitlab.com/ccsolutions.io/open-source/terraform/modules/ionos-dcd.git | 1.5.3 |
| <a name="module_k8s_cluster"></a> [k8s\_cluster](#module\_k8s\_cluster) | git::https://gitlab.com/ccsolutions.io/open-source/terraform/modules/ionos-k8s-cluster.git | 1.5.3 |

## Resources

| Name | Type |
|------|------|
| [helm_release.cert_manager](https://registry.terraform.io/providers/hashicorp/helm/2.5.1/docs/resources/release) | resource |
| [helm_release.nginx_ingress_controller](https://registry.terraform.io/providers/hashicorp/helm/2.5.1/docs/resources/release) | resource |
| [kubectl_manifest.clusterissuer_letsencrypt_http_prod](https://registry.terraform.io/providers/gavinbunney/kubectl/1.14.0/docs/resources/manifest) | resource |
| [kubectl_manifest.test](https://registry.terraform.io/providers/gavinbunney/kubectl/1.14.0/docs/resources/manifest) | resource |
| [local_sensitive_file.kubeconfig](https://registry.terraform.io/providers/hashicorp/local/2.2.2/docs/resources/sensitive_file) | resource |
| [ionoscloud_k8s_cluster.k8s_config](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.3.3/docs/data-sources/k8s_cluster) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment"></a> [environment](#input\_environment) | The name of the environment | `string` | n/a | yes |
| <a name="input_password"></a> [password](#input\_password) | Ionos Password for terraform. PLEASE DON'T USE YOUR PERSONAL PASSWORD. Credentials are stored in 1password | `string` | n/a | yes |
| <a name="input_username"></a> [username](#input\_username) | Ionos Username for terraform. PLEASE DON'T USE YOUR PERSONAL USER. Credentials are stored in 1password | `string` | n/a | yes |
| <a name="input_vcd_description"></a> [vcd\_description](#input\_vcd\_description) | A short description for the VDC | `string` | n/a | yes |
| <a name="input_vcd_location"></a> [vcd\_location](#input\_vcd\_location) | Location of the VDC | `string` | n/a | yes |
| <a name="input_vcd_name"></a> [vcd\_name](#input\_vcd\_name) | Name of the VDC | `string` | n/a | yes |
| <a name="input_vcd_sec_auth_protection"></a> [vcd\_sec\_auth\_protection](#input\_vcd\_sec\_auth\_protection) | Enforce 2FA on the VDC | `bool` | n/a | yes |
| <a name="input_web_app_test"></a> [web\_app\_test](#input\_web\_app\_test) | Domain for the webapp | `string` | n/a | yes |

## Outputs

No outputs.

[//]: # (END_TF_DOCS)