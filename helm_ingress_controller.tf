resource "helm_release" "nginx_ingress_controller" {
  name       = "nginx-ingress"
  chart      = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  version    = "4.0.16"
  namespace  = "ingress-nginx"
  timeout    = 600
  create_namespace = true

  values = [
    file("./helm_values/nginx-ingress.yaml")
  ]

  depends_on = [module.k8s_cluster]
}
