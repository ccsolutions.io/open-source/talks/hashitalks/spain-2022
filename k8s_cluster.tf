module "k8s_cluster" {
  source        = "git::https://gitlab.com/ccsolutions.io/open-source/terraform/modules/ionos-k8s-cluster.git?ref=1.5.3"
  username      = var.username
  password      = var.password
  datacenter_id = module.datacenter.vcd_id
  name          = "K8S-Cluster-001"
  k8s_version   = "1.24.6"
  environment   = var.environment
  maintenance_window = {
    day_of_the_week = "Monday"
    time            = "01:00:00Z"
  }
  node_pools = {
    node_pool_001 = {
      name = "node-pool-001"
      maintenance = {
        day_of_the_week = "Monday"
        time            = "02:30:00Z"
      }
      k8s_version = "1.24.6"
      auto_scaling = {
        min_node_count = 3
        max_node_count = 5
      }
      cpu_family        = "INTEL_SKYLAKE"
      availability_zone = "AUTO"
      storage_type      = "SSD"
      storage_size      = 30
      cores_count       = 4
      ram_size          = 4096
      annotations = {
        nodesgroup = "node-pool-01"
        workflows  = "all-tiers"
      }
      lans = {
        id   = module.datacenter.nodes_lan
        dhcp = true
      }
    }
  }
}
data "ionoscloud_k8s_cluster" "k8s_config" {
  id = module.k8s_cluster.id
}
resource "local_sensitive_file" "kubeconfig" {
  content  = yamlencode(jsondecode(data.ionoscloud_k8s_cluster.k8s_config.kube_config))
  filename = "${path.module}/kubeconfig.yaml"

  depends_on = [module.k8s_cluster]
}
