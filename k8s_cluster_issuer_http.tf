resource "kubectl_manifest" "clusterissuer_letsencrypt_http_prod" {
  yaml_body = <<-EOF
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt-http
    spec:
      ingressClassName: nginx
      acme:
        email: ssl@ccstech.dev
        privateKeySecretRef:
          name: letsencrypt-http
        server: https://acme-v02.api.letsencrypt.org/directory
        solvers:
          - http01:
              ingress:
                class: nginx
    EOF

  depends_on = [module.k8s_cluster, helm_release.cert_manager, helm_release.nginx_ingress_controller]
}
