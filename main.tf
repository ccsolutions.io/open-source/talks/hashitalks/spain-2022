terraform {
  required_version = "~> 1.5.3"
  required_providers {
    ionoscloud = {
      source  = "ionos-cloud/ionoscloud"
      version = "6.3.3"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.11.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.5.1"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.2"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }
  }
}
provider "ionoscloud" {
  username = var.username
  password = var.password
}

provider "kubernetes" {
  host                   = module.k8s_cluster.host
  token                  = module.k8s_cluster.token
  cluster_ca_certificate = module.k8s_cluster.ca
}
provider "helm" {
  kubernetes {
    host                   = module.k8s_cluster.host
    token                  = module.k8s_cluster.token
    cluster_ca_certificate = module.k8s_cluster.ca
  }
}
provider "kubectl" {
  host                   = module.k8s_cluster.host
  token                  = module.k8s_cluster.token
  cluster_ca_certificate = module.k8s_cluster.ca
  load_config_file       = false
}
