variable "username" {
  description = "Ionos Username for terraform. PLEASE DON'T USE YOUR PERSONAL USER. Credentials are stored in 1password"
  type        = string
  sensitive   = false
}
variable "password" {
  description = "Ionos Password for terraform. PLEASE DON'T USE YOUR PERSONAL PASSWORD. Credentials are stored in 1password"
  type        = string
  sensitive   = true
}
variable "environment" {
  description = "The name of the environment"
  type        = string
}
variable "vcd_name" {
  description = "Name of the VDC"
  type        = string
}
variable "vcd_location" {
  description = "Location of the VDC"
  type        = string
}
variable "vcd_description" {
  description = "A short description for the VDC"
  type        = string
}
variable "vcd_sec_auth_protection" {
  description = "Enforce 2FA on the VDC"
  type        = bool
}
variable "web_app_test" {
  description = "Domain for the webapp"
  type        = string
}
