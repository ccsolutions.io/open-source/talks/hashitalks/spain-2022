module "datacenter" {
  source              = "git::https://gitlab.com/ccsolutions.io/open-source/terraform/modules/ionos-dcd.git?ref=1.5.3"
  username            = var.username
  password            = var.password
  name                = var.vcd_name
  location            = var.vcd_location
  description         = var.vcd_description
  sec_auth_protection = var.vcd_sec_auth_protection
}
