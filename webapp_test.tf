resource "kubectl_manifest" "test_app" {
  sensitive_fields = [
    "metadata.annotations.my-secret-annotation"
  ]

  yaml_body = <<YAML
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: webapp
  name: webapp
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webapp
  template:
    metadata:
      labels:
        app: webapp
    spec:
      containers:
        - name: hello
          image: nginxdemos/hello
          ports:
            - containerPort: 80
          resources:
            limits:
              cpu: 100m
              memory: 256Mi
            requests:
              cpu: 50m
              memory: 128Mi
          livenessProbe:
            httpGet:
              path: /
              port: 80
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 1
            failureThreshold: 3
          readinessProbe:
            httpGet:
              path: /
              port: 80
            initialDelaySeconds: 10
            periodSeconds: 5
            timeoutSeconds: 1
            failureThreshold: 3
YAML

    depends_on = [module.k8s_cluster, helm_release.cert_manager, helm_release.nginx_ingress_controller]
}
resource "kubectl_manifest" "test_svc" {
  sensitive_fields = [
    "metadata.annotations.my-secret-annotation"
  ]

  yaml_body = <<YAML
---
apiVersion: v1
kind: Service
metadata:
  name: webapp-svc
  namespace: default
spec:
  clusterIP: None
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
  selector:
    app: webapp
YAML

  depends_on = [kubectl_manifest.test_app]
}
resource "kubectl_manifest" "test_ingress" {
  sensitive_fields = [
    "metadata.annotations.my-secret-annotation"
  ]

  yaml_body = <<YAML
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: webapp-ingress
  namespace: default
  annotations:
    cert-manager.io/tls-acme: 'true'
    cert-manager.io/cluster-issuer: letsencrypt-http
spec:
  ingressClassName: nginx
  tls:
    - hosts:
        - ${var.web_app_test}
      secretName: demo-app
  rules:
    - host: ${var.web_app_test}
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: webapp-svc
                port:
                  number: 80
YAML

  depends_on = [kubectl_manifest.test_svc]
}
